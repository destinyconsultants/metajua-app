	var currentUser = null;

	var app_name = 'Metajua',
	app_version = '0.0.1'
	app = new Framework7( {
	    modalTitle: app_name,
	    material: true,
		swipePanel: 'left',
		swipePanelCloseOpposite: true,
		panelsCloseByOutside: true,
		swipePanelOnlyClose: true,
		animatePages: false
	} ),
	pagination = {
		per_page: 1,
		page: 1
	},
	$$ = Dom7,
	db = window.openDatabase( 'metajua_app', app_version, 'Metajua App', 50000 );
	api_base = 'http://isaac.metajua.gq/app/public/';

window.localStorage.clear();

$$( '.app_name' ).text( app_name );

document.addEventListener( 'deviceready', function() {
	if ( window.localStorage.getItem( 'account_setup' ) == null )
		setup_account();
}, false );

function setup_account() {
	app.popup( '.popup-account', false, true );

	$$( '#account_submit' ).off( 'click' ).on( 'click', function( $event ) {
		var account_name = $$( '#account_name' );

		if ( account_name.val() == '' )
			account_name.focus();

		else {
			app.showPreloader();

			$$.post(
				api_base + 'account/validate',
				{
					account: account_name.val()
				},
				function( response ) {
					var response = JSON.parse( response );

					if ( response.result == 'successful' ) {
						window.localStorage.setItem( 'account_id', response.account_id );
						window.localStorage.setItem( 'account_setup', 1 );

						create_database_tables();

						download_users();
					}

					else {
						app.hidePreloader();

						app.alert( 'Account not known. Try again!' );
					}
				},
				function() {
					app.hidePreloader();

					app.alert( 'Server not accessible. Try again later!' );
				}
			);
		}
	} );
}

function create_database_tables() {
	db.transaction( function( tx ) {
		tx.executeSql( 'CREATE TABLE users ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, username CHAR(20), password CHAR(60) )', [] );
		tx.executeSql( 'CREATE TABLE user_meta ( id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, meta_key CHAR(250), meta_value TEXT )', [] );
	} );
}

function download_users() {
	if ( pagination.page == 1 ) {
		truncate_table( 'users' );

		truncate_table( 'user_meta' );
	}

	$$.post(
		api_base + 'users',
		{
			account: window.localStorage.getItem( 'account_id' ),
			per_page: pagination.per_page,
			page: pagination.page
		},
		function( response ) {
			var response = JSON.parse( response );

			if ( response.result == 'successful' ) {
				db.transaction( function( tx ) {
					$$.each( response.users, function( index, user ) {
						if ( user.meta.has_mobile_app_access == '1' ) {
							tx.executeSql(
								'INSERT INTO users ( remote_id, username, password ) VALUES ( ?, ?, ? )',
								[ user.id, user.username, user.meta.mobile_app_password ],
								function( tx, result ) {
									var user_meta = Object.assign( {
											first_name: user.name.first,
											last_name: user.name.last
										}, user.meta ),
										user_id = result.insertId;

									$$.each( user_meta, function( meta_key, meta_value ) {
										update_user_meta( user_id, meta_key, meta_value );
									} );
								}
							);
						}
					} );
				} );

				pagination.page = ( response.users.length < pagination.per_page ) ? 1 : ( pagination.page + 1 );

				if ( pagination.per_page == response.users.length )
					download_users();

				else {
					app.hidePreloader();

					app.closeModal( '.popup-account', true );
				}
			}

			else
				download_users();
		},
		function() {
			app.hidePreloader();

			app.alert( 'Server not accessible. Try again later!' );
		}
	);
}

function update_user_meta( user_id, meta_key, meta_value ) {
	user_id = typeof user_id == 'undefined' ? 0 : parseInt( user_id );
	meta_value = typeof meta_value == 'undefined' ? '' : meta_value;

	if ( typeof meta_key != 'undefined' ) {
		db.transaction( function( tx ) {
			tx.executeSql(
				'SELECT id FROM user_meta WHERE user_id = ? AND meta_key = ? LIMIT 1',
				[ user_id, meta_key ],
				function( tx, result ) {
					if ( result.rows.length == 1 )
						tx.executeSql(
							'UPDATE user_meta SET meta_value = ? WHERE id = ?',
							[ meta_value, result.rows.item( 0 ).id ]
						);

					else
						tx.executeSql(
							'INSERT INTO user_meta ( user_id, meta_key, meta_value ) VALUES ( ?, ?, ? )',
							[ user_id, meta_key, meta_value ],
							null,
							function( tx, error ) {
								app.alert( error.message );
							}
						);
				}
		 	);
		} );
	}
}

function truncate_table( table ) {
	db.transaction( function( tx ) {
		tx.executeSql( 'DELETE FROM ' + table, [] );
	} );
}

function show_table_records( table ) {
	table = typeof table == 'undefined' ? 'users' : table;

	db.transaction( function( tx ) {
		tx.executeSql( 'SELECT * FROM ' + table, [], function( tx, result ) {
			for ( i = 0; i < result.rows.length; i++ )
				console.log( result.rows.item( i ) );
		} );
	} )
}




function openLoginScreen() {
	$$( '.login-screen' ).css( {
		'margin-top' : parseInt( ( $$( window ).height() - 250 ) / 2 ) + 'px'
	} );

//	$$( '.login-screen-content form [name="username"], .login-screen-content form [name="password"]' ).val( '' );

	$$( '#login_submit' ).off( 'click' ).on( 'click', function( event ) {
		//event.preventDefault();

		var username = $$( 'input#username' ),
			password = $$( 'input#password' );

		if ( username.val().trim().length == 0 )
		 	username.focus();

		else if ( password.val().trim().length == 0 )
			password.focus();

		else {
			app.showPreloader();

			db.transaction( function( tx ) {
				tx.executeSql(
					'SELECT * FROM users WHERE username = ? LIMIT 1',
					[ username.val() ],
					function( tx, result ) {
						if ( result.rows.length > 0 ) {
							tx.executeSql(
								'SELECT * FROM users WHERE username = ? AND password = ? LIMIT 1',
								[ username.val(), password.val() ],
								function( tx, result ) {
									app.hidePreloader();

									if ( result.rows.length > 0 ) {
										var record = result.rows.item( 0 );

										currentUser.id = record.id;
										currentUser.remote_id = record.remote_id;
										currentUser.name.first = record.first_name;
										currentUser.name.last = record.last_name;
										currentUser.name.user = record.user_name;
										currentUser.districts = JSON.parse( record.district_ids );

										window.localStorage.setItem( 'currentUser', JSON.stringify( currentUser ) );

										if ( window.localStorage.getItem( 'technicianSetup' ) == null )
											setupTechnician();

										setTimeout( function() {
											app.closeModal( '#login-screen', true );
										}, 2000 );
									}

									else {
										app.hidePreloader();

										app.alert( 'Wrong password entered. Try again!' );
									}
								},
								null
							);
						}

						else {
							app.hidePreloader();

							app.alert( 'No technician found matching that username. Try again!' );
						}
					},
					null
				);
			} );
		}
	} );

	app.loginScreen( '#login-screen', true );
}
